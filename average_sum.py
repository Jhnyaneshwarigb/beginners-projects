#!/usr/bin/env python
# coding: utf-8

# In[10]:


def average (num):
    total_num = 0
    for t in num:
        total_num += t
    average = total_num / len(num)
    return average
 


# In[11]:


print("The average is", average([18,25,3,41,5]))


# In[7]:


def cal_average(num):
    sum_num = 0
    for t in num:
        sum_num = sum_num + t           

    avg = sum_num / len(num)
    return avg

print("The average is", cal_average([18,25,3,41,5]))


# In[ ]:




