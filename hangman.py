import random
from words import words

def get_word():
    word = random.choice(words)
    return word.upper()

def play(word):
    word_completion = "-" * len(word)
    guessed = False
    guessed_letters =[]
    guessed_words = []
    tries = 6
    print("let's play hangman")
    print(word_completion)
    print("\n")
    while not guessed and tries > 0:
        guess = input("guess a letter or word:").upper()
        if len(guess) == 1 and guess.isalpha():
            if guess in guessed_letters:
                print("you already guessed the letter", guess)
            elif guess not in word:
                print(guess, "is not in the word")
                tries -= 1
                guessed_letters.append(guess)
            else:
                print("Nice job", guess, "is the word.")
                guessed_letters.append(guess)
                word_list = list(word_completion)
                indices = [i for i, letter in enumerate(word) if letter == guess]
                for index in indices:
                    word_list[index] = guess
                word_completion = "".join(word_list)
                if "_" not in word_completion:
                    guessed = True
        elif len(guess) == len(word) and guess.isalpha():
            if guess in guessed_words:
                print("you already guessed the word", guess)
            elif guess!= word:
                print(guess,":is not in the word." )
                tries -= 1
                guessed_words.append(guess)
            else:
                guessed = True
                word_completion = word

        else:
            print("Not a valid guess")
            print(word_completion)
            if guessed:
                print("Great, You win!")
            else:
                print("sorry you ran out of tries,correct word was", word)
def main():
    word = get_word()
    play(word)
    while input("play again? (y/n)").lower() == "y":
        word = get(word)
        play(word)

if __name__ == "__main__":
    main()

