#!/usr/bin/env python
# coding: utf-8

# In[21]:



def binary_search(sequence, num):
    lower_limit = 0
    upper_limit = len(sequence)-1
    
    while lower_limit<= upper_limit:
        midpoint = (lower_limit + upper_limit)//2
        mid_value = sequence[midpoint]
        
        if mid_value == num:
            return midpoint
        else:
            if mid_value > num:
                lower_limit = midpoint +1
            else:
                upper_limit = midpoint -1             
    return False                       


# In[22]:


sequence = [1,2,3,4,5,6,7,8,9,10]
num = 5
print(binary_search(sequence, num))


# In[ ]:




